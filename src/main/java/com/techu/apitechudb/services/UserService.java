package com.techu.apitechudb.services;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(){
        System.out.println("findAll en UserService");

        return this.userRepository.findAll();
    }

    public Optional<UserModel> findByAge(int age){
        System.out.println("findByAge en UserService");

        return this.userRepository.findByAge(age);
    }

    public Optional<UserModel> findById(String id){
        System.out.println("findById en UserService");

        return this.userRepository.findById(id);
    }

    public UserModel add(UserModel user){
        System.out.println("add en UserService");

        return this.userRepository.save(user);
    }

    public UserModel update(UserModel user){
        System.out.println("update en UserService");

        return this.userRepository.update(user);
    }

    public boolean delete(String id){
        System.out.println("delete en UserService");

        boolean result = false;

        Optional<UserModel> userToDelete = this.findById(id);

        if (userToDelete.isPresent() == true){
            result = true;
            this.userRepository.delete(userToDelete.get());
        }

        return result;
    }

    public Optional<UserModel> getById(String id) {
        System.out.println("getById en userService");
        System.out.println("La id es " + id);

        return this.userRepository.findById(id);
    }
}
