package com.techu.apitechudb;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class ApitechudbApplication {

	public static ArrayList<ProductModel> productModels;

	public static ArrayList<UserModel> userModels;

	public static ArrayList<PurchaseModel> purchaseModels;

/*	public static void main(String[] args) {
		SpringApplication.run(ApitechudbApplication.class, args);

		ApitechudbApplication.productModels = ApitechudbApplication.getTestData();
	}*/

	public static void main(String[] args) {
		SpringApplication.run(ApitechudbApplication.class, args);

		ApitechudbApplication.userModels = ApitechudbApplication.getTestDataUser();
		ApitechudbApplication.productModels = ApitechudbApplication.getTestDataProduct();
		ApitechudbApplication.purchaseModels = ApitechudbApplication.getTestDataPurchase();
	}

	private static ArrayList<ProductModel> getTestDataProduct(){

		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel(
						"1",
						"Producto 1",
						10
				)
		);

		productModels.add(
				new ProductModel(
						"2",
						"Producto 2",
						20
				)
		);

		productModels.add(
				new ProductModel(
						"3",
						"Producto 3",
						30
				)
		);

		productModels.add(
				new ProductModel(
						"4",
						"Producto 4",
						40
				)
		);

		return productModels;
	}

	private static ArrayList<UserModel> getTestDataUser(){

		ArrayList<UserModel> userModels = new ArrayList<>();

		userModels.add(
				new UserModel(
						"1",
						"Nombre 1",
						10
				)
		);

		userModels.add(
				new UserModel(
						"2",
						"Nombre 2",
						20
				)
		);

		userModels.add(
				new UserModel(
						"3",
						"Nombre 3",
						30
				)
		);

		return userModels;
	}

	private static ArrayList<PurchaseModel> getTestDataPurchase(){

//		return new ArrayList<>();

		ArrayList<PurchaseModel> purchaseModels = new ArrayList<>();

		HashMap compra1 = new HashMap();
		compra1.put("1",1);
		compra1.put("2",2);

		HashMap compra2 = new HashMap();
		compra2.put("2",2);
		compra2.put("3",3);

		HashMap compra3 = new HashMap();
		compra3.put("3",3);
		compra3.put("4",4);

		purchaseModels.add(
				new PurchaseModel(
						"1",
						"1",
						50,
						compra1
				)
		);

		purchaseModels.add(
				new PurchaseModel(
						"2",
						"2",
						130,
						compra2
				)
		);

		purchaseModels.add(
				new PurchaseModel(
						"3",
						"3",
						250,
						compra3
				)
		);

		return purchaseModels;
	}

}
