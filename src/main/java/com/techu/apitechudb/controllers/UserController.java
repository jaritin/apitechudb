package com.techu.apitechudb.controllers;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v3")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(value = "age",defaultValue = "0") int age) {
        System.out.println("getUsers");

        if (age != 0){
            List<UserModel> result = new ArrayList<UserModel>();

            for(UserModel userInList : ApitechudbApplication.userModels){
                if (userInList.getAge() == age){
                    result.add(userInList);
                }

            }
            return new ResponseEntity<>(
                    result,
                    HttpStatus.OK
            );
        }
        else{
            return new ResponseEntity<>(
                    this.userService.findAll(),
                    HttpStatus.OK
            );
        }
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("La id del usuario a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("La id del usuario a crear es " + user.getId());
        System.out.println("El nombre del usuario a crear es " + user.getName());
        System.out.println("La edad del usuario a crear es " + user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id){
        System.out.println("updateUser");
        System.out.println("La id del usuario a actualizar en parámetro es " + id);
        System.out.println("La id del usuario a actualizar es " + user.getId());
        System.out.println("El nombre del usuario a actualizar es " + user.getName());
        System.out.println("La edad del usuario a actualizar es " + user.getAge());

        return new ResponseEntity<>(this.userService.update(user), HttpStatus.OK);

    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUser");
        System.out.println("La id del usuario a borrar en parámetro es " + id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario borrado" : "Usuario no encontrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
