package com.techu.apitechudb.models;

import java.util.Map;

public class PurchaseModel {

    private String purchaseId;
    private String userId;
    private float amount;
    private Map<String,Integer> purchaseItems;


    public PurchaseModel() {
    }

    public PurchaseModel(String purchaseId, String userId, float amount, Map<String, Integer> purchaseItems) {
        this.purchaseId = purchaseId;
        this.userId = userId;
        this.amount = amount;
        this.purchaseItems = purchaseItems;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Map<String, Integer> getPurchaseItems() {
        return purchaseItems;
    }

    public void setPurchaseItems(Map<String, Integer> purchaseItems) {
        this.purchaseItems = purchaseItems;
    }
}
