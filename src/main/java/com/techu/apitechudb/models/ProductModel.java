package com.techu.apitechudb.models;

public class ProductModel {

    private String id;
    private String desc;
    private int price;

    public ProductModel() {
    }

    public ProductModel(String id, String desc, int price) {
        this.id = id;
        this.desc = desc;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
